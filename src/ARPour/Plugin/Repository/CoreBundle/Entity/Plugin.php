<?php
namespace ARPour\Plugin\Repository\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WhereGroup\MetadorBundle\Entity\Address
 *
 * @ORM\Table(name="plugins")
 * @ORM\Entity
 */
class Plugin
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="text")
     */
    protected $path;

    /**
     * @ORM\Column(type="text")
     */
    protected $hash;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $version;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $major_release;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $minor_release;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $patch_level;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $build_number;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Plugin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Plugin
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Plugin
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Plugin
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return Plugin
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set major_release
     *
     * @param integer $majorRelease
     * @return Plugin
     */
    public function setMajorRelease($majorRelease)
    {
        $this->major_release = $majorRelease;

        return $this;
    }

    /**
     * Get major_release
     *
     * @return integer
     */
    public function getMajorRelease()
    {
        return $this->major_release;
    }

    /**
     * Set minor_release
     *
     * @param integer $minorRelease
     * @return Plugin
     */
    public function setMinorRelease($minorRelease)
    {
        $this->minor_release = $minorRelease;

        return $this;
    }

    /**
     * Get minor_release
     *
     * @return integer
     */
    public function getMinorRelease()
    {
        return $this->minor_release;
    }

    /**
     * Set patch_level
     *
     * @param integer $patchLevel
     * @return Plugin
     */
    public function setPatchLevel($patchLevel)
    {
        $this->patch_level = $patchLevel;

        return $this;
    }

    /**
     * Get patch_level
     *
     * @return integer
     */
    public function getPatchLevel()
    {
        return $this->patch_level;
    }

    /**
     * Set build_number
     *
     * @param integer $buildNumber
     * @return Plugin
     */
    public function setBuildNumber($buildNumber)
    {
        $this->build_number = $buildNumber;

        return $this;
    }

    /**
     * Get build_number
     *
     * @return integer
     */
    public function getBuildNumber()
    {
        return $this->build_number;
    }
}
