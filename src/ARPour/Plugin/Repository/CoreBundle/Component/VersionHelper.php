<?php

namespace ARPour\Plugin\Repository\CoreBundle\Component;

use ARPour\Plugin\Repository\CoreBundle\Entity\Plugin;
use Symfony\Component\DependencyInjection\Container;

class VersionHelper implements VersionHelperInterface
{
    public function explodeVersion($version)
    {
        $versionArray = array();
        $count = count(explode('.', trim($version)));

        switch($count) {
            case 1:
                list(
                    $versionArray['major_release']
                ) = explode('.', trim($version));
                break;
            case 2:
                list(
                    $versionArray['major_release'],
                    $versionArray['minor_release']
                ) = explode('.', trim($version));
                break;
            case 3:
                list(
                    $versionArray['major_release'],
                    $versionArray['minor_release'],
                    $versionArray['patch_level']
                ) = explode('.', trim($version));
                break;
            case 4:
                list(
                    $versionArray['major_release'],
                    $versionArray['minor_release'],
                    $versionArray['patch_level'],
                    $versionArray['build_number']
                ) = explode('.', trim($version));
                break;
        }

        return $versionArray;
    }

    public function implodeVersion($major_release, $minor_release, $patch_level, $build_number)
    {
        $version = trim($major_release);

        if ($minor_release !== null) {
            $version .= '.' . trim($minor_release);

            if ($patch_level !== null) {
                $version .= '.' . trim($patch_level);

                if ($build_number !== null) {
                    $version .= '.' . trim($build_number);

                }
            }
        }

        return $version;
    }
}
