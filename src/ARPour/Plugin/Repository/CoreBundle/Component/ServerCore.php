<?php

namespace ARPour\Plugin\Repository\CoreBundle\Component;

use ARPour\Plugin\Repository\CoreBundle\Entity\Plugin;
use Symfony\Component\DependencyInjection\Container;
use ARPour\Plugin\Repository\CoreBundle\Component\VersionHelperInterface;

class ServerCore
{
    protected $container;
    protected $versionHelper;
    protected $em;
    protected $repository;

    /**
     * Constructor
     * @param Container $container
     */
    public function __construct(Container $container, VersionHelperInterface $versionHelper)
    {
        $this->container     = $container;
        $this->versionHelper = $versionHelper;
        $this->repository    = 'ARPourPluginRepositoryCoreBundle:Plugin';
        $this->em            = $this->container->get('doctrine')->getManager();
    }

    public function addPlugin($data)
    {
        $version = $this->versionHelper->implodeVersion(
            $data['major_release'],
            $data['minor_release'],
            $data['patch_level'],
            $data['build_number']
        );

        try {
            $this->getPlugin($data['name'], $version);
        } catch (\RuntimeException $e) {
            $plugin = new Plugin();
            $plugin->setVersion($version);

            if (isset($data['name'])) {
                $plugin->setName($data['name']);
            }
            if (isset($data['description'])) {
                $plugin->setDescription($data['description']);
            }
            if (isset($data['major_release'])) {
                $plugin->setMajorRelease($data['major_release']);
            }
            if (isset($data['minor_release'])) {
                $plugin->setMinorRelease($data['minor_release']);
            }
            if (isset($data['patch_level'])) {
                $plugin->setPatchLevel($data['patch_level']);
            }
            if (isset($data['build_number'])) {
                $plugin->setBuildNumber($data['build_number']);
            }
            if (isset($data['path'])) {
                $plugin->setPath($data['path']);
            }
            if (isset($data['path'])) {
                $plugin->setHash(sha1_file($data['path']));
            }

            $this->em->persist($plugin);
            $this->em->flush();

            return $plugin->getId();
        }

        throw new \RuntimeException('Plugin already exists.');
    }

    /**
     * Remove a plugin version or all versions.
     * @param  string $name    Plugin name
     * @param  string $version Version
     */
    public function removePlugin($name, $version = null)
    {
        $plugins = $this->getRawPlugin($name, $version, null);

        foreach ($plugins as $plugin) {
            $this->em->remove($plugin);
        }

        $this->em->flush();
    }

    /**
     * Get a plugin version or n versions from a plugin.
     * @param  string  $name    Plugin name
     * @param  string  $version Version
     * @param  integer $limit   Limit
     * @return array
     */
    public function getPlugin($name, $version = null, $limit = 1)
    {
        $qb = $this->em
            ->getRepository($this->repository)
            ->createQueryBuilder('p');

        if ($version === null) {
            $qb->where(
                $qb->expr()->eq('p.name', '?1')
            )->setParameter(1, trim($name));
        } else {
            $qb->where($qb->expr()->andx(
                $qb->expr()->eq('p.name', '?1'),
                $qb->expr()->eq('p.version', '?2')
            ))->setParameters(array(
                1 => trim($name),
                2 => trim($version)
            ));
        }

        $query = $qb
            ->addOrderBy('p.major_release', 'DESC')
            ->addOrderBy('p.minor_release', 'DESC')
            ->addOrderBy('p.patch_level', 'DESC')
            ->addOrderBy('p.build_number', 'DESC')
            ->getQuery();

        if ($limit === false) {
            $plugin = $query
                ->getResult();
        } else {
            $plugin = $query
                ->setFirstResult(0)
                ->setMaxResults($limit)
                ->getResult();
        }

        if (!$plugin) {
            throw new \RuntimeException('Plugin not found.');
        }

        return $plugin;
    }

    /**
     * Get all plugins.
     * @param  boolean $allVersions Return all plugins in all versions.
     * @return array    Array of Plugins
     */
    public function getPlugins($allVersions = false)
    {
        $qb = $this->em
            ->getRepository($this->repository)
            ->createQueryBuilder('p');

        if ($allVersions === false) {
            $qb->groupBy('p.name');
        }

        return $qb
            ->orderBy('p.name', 'ASC')
            ->addOrderBy('p.major_release', 'DESC')
            ->addOrderBy('p.minor_release', 'DESC')
            ->addOrderBy('p.patch_level', 'DESC')
            ->addOrderBy('p.build_number', 'DESC')
            ->getQuery()
            ->getResult();
    }



    public function checkForUpdates($plugins)
    {
        $qb = $this->em
            ->getRepository($this->repository)
            ->createQueryBuilder('p')
            ->groupBy('p.name')
            // ->orderBy('p.name', 'ASC')
            // ->addOrderBy('p.major_release', 'DESC')
            // ->addOrderBy('p.minor_release', 'DESC')
            // ->addOrderBy('p.patch_level'  , 'DESC')
            // ->addOrderBy('p.build_number' , 'DESC')
            ;

        $i = 0;

        foreach ($plugins as $name => $version) {
            ++$i;

            $versionArray = $this->versionHelper->explodeVersion($version);

            switch(count($versionArray)) {
                case 1:
                    throw new \RuntimeException('You can not update this!');

                    break;
                case 2:
                    // TODO:  TESTEN UND UMSETZEN
                case 3:
                    // TODO:  TESTEN UND UMSETZEN
                    $qb->orWhere(
                        $qb->expr()->andx(
                            $qb->expr()->eq('p.name', '?' . $i),
                            $qb->expr()->neq('p.version', ':version'),
                            $qb->expr()->eq('p.major_release', ':major_release'),
                            $qb->expr()->gte('p.minor_release', ':minor_release')
                        )
                    )
                    ->setParameter($i, trim($name))
                    ->setParameter('version', $version)
                    ->setParameter('major_release', $versionArray['major_release'])
                    ->setParameter('minor_release', $versionArray['minor_release']);

                    break;
                case 4:
                    $qb->orWhere(
                        $qb->expr()->andx(
                            $qb->expr()->eq('p.name', '?' . $i),
                            $qb->expr()->neq('p.version', ':version'),
                            $qb->expr()->eq('p.major_release', ':major_release'),
                            $qb->expr()->eq('p.minor_release', ':minor_release'),
                            $qb->expr()->orx(
                                $qb->expr()->gt('p.patch_level', ':patch_level'),
                                $qb->expr()->andx(
                                    $qb->expr()->eq('p.patch_level', ':patch_level'),
                                    $qb->expr()->gt('p.build_number', ':build_number')
                                )
                            )
                        )
                    )
                    ->setParameter($i, trim($name))
                    ->setParameter('version', $version)
                    ->setParameter('major_release', $versionArray['major_release'])
                    ->setParameter('minor_release', $versionArray['minor_release'])
                    ->setParameter('patch_level', $versionArray['patch_level'])
                    ->setParameter('build_number', $versionArray['build_number']);

                    break;
            }

        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function executeCommand($command)
    {
        $this->container->get('event_dispatcher')->dispatch(
            'repository.command',
            $command
        );

        return $command->getResult();
    }
}
