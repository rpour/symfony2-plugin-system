<?php

namespace ARPour\Plugin\Repository\CoreBundle\Component\Command;

use Symfony\Component\EventDispatcher\Event;

class Command extends Event
{
    protected $result;

    public function __construct($request)
    {
        foreach ($request as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Gets the value of result.
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Sets the value of result.
     *
     * @param mixed $result the result
     *
     * @return self
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }
}
