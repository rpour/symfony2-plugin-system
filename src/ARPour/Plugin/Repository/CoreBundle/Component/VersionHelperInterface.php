<?php

namespace ARPour\Plugin\Repository\CoreBundle\Component;

interface VersionHelperInterface
{
    public function explodeVersion($version);
    public function implodeVersion($major_release, $minor_release, $patch_level, $build_number);
}
