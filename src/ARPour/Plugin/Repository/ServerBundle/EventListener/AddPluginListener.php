<?php

namespace ARPour\Plugin\Repository\ServerBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use ARPour\Plugin\Repository\ServerBundle\Component\Command\AddPlugin;

class AddPluginListener
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onRepositoryCommand(AddPlugin $command)
    {
        $server  = $this->container->get('arpour_plugin_repo_server');
        $request = $this->container->get('request');

        // $id = $server->addPlugin(array(
        //     'name'          => $command->getName(),
        //     'description'   => $command->getDescription(),
        //     'major_release' => $command->getMajorRelease(),
        //     'minor_release' => $command->getMinorRelease(),
        //     'patch_level'   => $command->getPatchLevel(),
        //     'build_number'  => $command->getBuildNumber(),
        //     'path'          => $command->getPath(),
        // ));

        $id = "2323";

        $command->setResult(array(
            'status' => 'ok',
            'id'     => $id
        ));
    }
}
