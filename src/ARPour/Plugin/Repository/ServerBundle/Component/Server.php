<?php

namespace ARPour\Plugin\Repository\ServerBundle\Component;

use ARPour\Plugin\Repository\CoreBundle\Entity\Plugin;
use ARPour\Plugin\Repository\CoreBundle\Component\ServerCore;

use Symfony\Component\DependencyInjection\Container;

class Server extends ServerCore
{
    public function version()
    {
        return '1.0';
    }

    // TODO: user serializer
    public function addPlugin($data)
    {
        // TODO: move to ServerCore
        if (!$data['path'] || !file_exists($data['path']) || !is_readable($data['path'])) {
            throw new \RuntimeException('File not found.');
        }

        return parent::addPlugin($data);
    }

    // TODO: user serializer
    public function getPlugin($name, $version = null, $limit = 1)
    {
        return $this->serializePlugins(
            parent::getPlugin($name, $version, $limit)
        );
    }

    // TODO: user serializer
    public function getPlugins($allVersions = false)
    {
        return $this->serializePlugins(
            parent::getPlugins($allVersions)
        );
    }

    // TODO: user serializer
    public function checkForUpdates($plugins)
    {
        return $this->serializePlugins(
            parent::checkForUpdates($plugins)
        );
    }

    // TODO: write serializer
    private function serializePlugins($rawPlugins)
    {
        $plugins = array();

        foreach ($rawPlugins as $plugin) {
            $plugins[] = array(
                'name' => $plugin->getName(),
                'description' => $plugin->getDescription(),
                'version' => $plugin->getVersion()
            );
        }

        return $plugins;
    }
}
