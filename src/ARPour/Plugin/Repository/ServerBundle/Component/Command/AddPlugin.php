<?php

namespace ARPour\Plugin\Repository\ServerBundle\Component\Command;

use ARPour\Plugin\Repository\CoreBundle\Component\Command\Command;

class AddPlugin extends Command
{
    protected $name;
    protected $description;
    protected $majorRelease;
    protected $minorRelease;
    protected $patchLevel;
    protected $buildNumber;
    protected $path;

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Gets the value of majorRelease.
     *
     * @return mixed
     */
    public function getMajorRelease()
    {
        return $this->majorRelease;
    }

    /**
     * Gets the value of minorRelease.
     *
     * @return mixed
     */
    public function getMinorRelease()
    {
        return $this->minorRelease;
    }

    /**
     * Gets the value of patchLevel.
     *
     * @return mixed
     */
    public function getPatchLevel()
    {
        return $this->patchLevel;
    }

    /**
     * Gets the value of buildNumber.
     *
     * @return mixed
     */
    public function getBuildNumber()
    {
        return $this->buildNumber;
    }

    /**
     * Gets the value of path.
     *
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }
}
