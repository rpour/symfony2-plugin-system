<?php

namespace ARPour\Plugin\Repository\ServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use ARPour\Plugin\Repository\CoreBundle\Entity\Plugin;
use ARPour\Plugin\Repository\ServerBundle\Component\Command;

/**
 * @Route("/api/repo")
 */
class ServerController extends Controller
{
    /**
     * @Route("/", name="repo_server_index")
     */
    public function indexAction()
    {
        return new JsonResponse(array(
            'version' => $this->container->get('arpour_plugin_repo_server')
                ->version()
        ));
    }

    /**
     * @Route("/get/{name}", name="repo_server_get")
     * @Method("GET")
     */
    public function getAction($name)
    {
        return new JsonResponse(
            $this->container->get('arpour_plugin_repo_server')
                ->getPlugin($name)
        );
    }

    /**
     * @Route("/getall", name="repo_server_getall")
     * @Method("GET")
     */
    public function getAllAction()
    {
        return new JsonResponse(
            $this->container->get('arpour_plugin_repo_server')
                ->getAllPlugins()
        );
    }

    /**
     * @Route("/add", name="repo_server_add")
     * @Method("POST")
     */
    public function addAction()
    {
        return new JsonResponse(
            $this->container->get('arpour_plugin_repo_server')
            ->executeCommand(
                new Command\AddPlugin($this->getRequest()->request->all())
            )
        );
    }
}
