<?php

namespace ARPour\Plugin\Repository\ServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use ARPour\Plugin\Repository\CoreBundle\Entity\Plugin;
use ARPour\Plugin\Repository\ServerBundle\Component\Server;

/**
 * @Route("/test")
 */
class TestController extends Controller
{

   /**
     * @Route("/a")
     * @Method("GET")
     */
    public function aAction()
    {
        $server  = $this->container->get('arpour_plugin_repo_server');
        $client  = $this->container->get('arpour_plugin_repo_client');

        // $server->remove('fake-v-1-3');

        // $plugin = $server->get('fake-v-1-3');
        // die('<pre>' . print_r($plugin, 1) . '</pre>');

        $plugins = $server->getPlugins(true);
        // $plugins = $server->getPlugin('fake-v-1-3', null, false);
        echo '<pre>' . print_r($plugins, 1) . '</pre>';


        $plugins = $server->checkForUpdates(array(
            'default-theme' => '1.1.0.0',
            // 'user-group' => '1.0',
        ));

        echo '<hr/>';
        echo '<pre>' . print_r($plugins, 1) . '</pre>';

        die;
    }
}
